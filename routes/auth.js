'use strict';
const Account = require('../models/account');
const uuidv4 = require('uuid/v4');

module.exports = (app, root_path) => {
    const local_root = root_path + '/auth';

    app.post(local_root + '/login',
        (req, res) => {
            Account.findOne({email: new RegExp('^' + req.body['email'] + '$', 'i')},
                (err, account) => {
                    if (err) {
                        return res.sendStatus(500);
                    }
                    if (!account) {
                        return res.sendStatus(404);
                    }
                    if (account.password !== req.body['password']) {
                        return res.sendStatus(401);
                    }

                    account.sessionToken = uuidv4();
                    account.save(
                        (err) => {
                            if (err) {
                                return res.sendStatus(500);
                            }
                            return res.status(200).json({'id': account._id, 'sessionToken': account.sessionToken});
                        });
                });
        });

    app.post(local_root + '/register',
        (req, res) => {

            Account.findOne({email: new RegExp('^' + req.body['email'] + '$', 'i')},
                (err, account) => {
                    if (account !== null) {
                        return res.status(409).json({'error': 'An account with this email already exists'});
                    }

                    Account.findOne({username: new RegExp('^' + req.body['username'] + '$', 'i')},
                        (err, account) => {
                            if (account !== null) {
                                return res.status(409).json({'error': 'An account with this username already exists'});
                            }

                            account = new Account(req.body);
                            account.save(
                                (err) => {
                                    if (err) {
                                        return res.status(400).json({'error': err});
                                    }

                                    return res.status(201).json({'sessionToken': account.sessionToken});
                                });
                        });
                });
        });
};