# Salem Authentication Service
## What is it?
salem-auth is a backend authentication service for salem-gdk. This service manages session, registrations, and logins for user accounts.

## API
### Login

url: ```http://<host>:<port>/api/auth/login```

requests:
```
{
    "email":    "The email of the login",
    "password": "the password of the login"
}
```

responses:
```
{
    "id": "The unique id of the related Account Object",
    "sessionToken": "A Unique Id generated at each login request, used to manage sessions"
}
```

statuses:
 - 200: If the login was successful
 - 401: If the service found an account associated with the requested email, but the password does not match
 - 404: If the service could not find an account associated with the requested email
 - 500: If the login could not properly read or save a value


### Register

url: http://<host>:<port>/api/auth/register

requests:
```
{
    "email":    "The email of the New Account",
    "username": "The username of the new account"
    "password": "The password of the new account"
}
```

responses:
```
{
    "id": "The unique id of the related Account Object",
    "sessionToken": "A Unique Id generated at each login request, used to manage sessions"
}
```

statuses:
 - 201: If the account creation was successful
 - 400: If request is not in the correct format, or there is a problem with database connection
 - 409: If the email or username of the request is already in use