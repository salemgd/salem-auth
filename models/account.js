'use strict';
const mongoose = require('mongoose');
const uuidv4 = require('uuid/v4');

class Account extends mongoose.Schema {
    constructor() {
        return super ({
            username: {
                type: String,
                required: true,
                unique: true
            },
            email: {
                type: String,
                required: true,
                unique: true
            },
            password: {
                type: String,
                required: true
            },
            sessionToken: {
                type: String,
                default: uuidv4(),
                unique: true
            },
            dateCreated: {
                type: Date,
                default: Date.now(),
                required: true
            }
        });
    }
}

module.exports = mongoose.model('account', new Account());